# PDS-mq
This is the patch repository for PDS-mq(Priority and Deadline based Skiplist multiple queue Scheduler) cpu scheduler.

The patches can apply upon mainline linux kernel tree, and categoried by kernel release. This repository can also be used for bug trace. Please open issue for PDS-mq in this repository, and remind me at PDS-mq development blog( * https://cchalpha.blogspot.com ).

Below is the benchmark provided by user for your reference.
* https://docs.google.com/spreadsheets/d/163U3H-gnVeGopMrHiJLeEY1b7XlvND2yoceKbOvQRm4/edit#gid=1458203995
